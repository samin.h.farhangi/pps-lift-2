**PPS-LIFT Project**
**README: Part-II**

**eGWAS Analysis Pipeline** 

- **Author:** Samin HF  
- **Date:** 27/02/2022  

---

## Pipeline Overview
This pipeline provides a systematic procedure for eGWAS (expression Genome-Wide Association Study) analysis. It begins with genotype preparation and concludes with the annotation of the results.
Before executing all steps, adjustments to eGWAS_analysis.sh are necessary. The current directories and codes are tailored for the second part of the PPS-LIFT project, a collaboration between the Animal Breeding and Genomics (ABG) group at Wageningen University & Research and Topigs Norsvin.
After modifying the bash file, execute it:

```bash
git clone git@gitlab.com:samin.h.farhangi/pps-lift-2.git
cd pps-lift-2
sbatch eGWAS_analysis.sh
```

The steps are as follows:

1. Genotype Preparation.
2. RNASeq File Preparation.
3. eGWAS Analysis.
4. Filtering and Annotating eGWAS Results.
5. eQTL and Gene Hotspots.

---

### Step 1: Genotype Preparation

#### 1A: Genotype QC and Information Update
- Execute the `plink.sh` script:
  ```bash
  sbatch plink.sh
  ```

- Filter genotypes for each chromosome:
  ```bash
  DATA=<Path to CHR.txt> # This should include chromosome numbers 1-18 and 23
  for i in `cat $DATA`
  do
    echo $i
    sbatch plink_filter.sh $i
  done
  ```

#### 1B: PCA Analysis for Genotyping
- Prepare PCA files:
  ```bash
  dir_genotype=<Path to genotypes>
  plink --bfile $dir_genotype/QC_100samples_filt2 --noweb --pca --out $dir_genotype/QC_100samples_filt_PCA
  ```

- Analyze PCA to determine the relationship between individuals. The code for this step can be found in `eGWAS_analysis.sh`.

#### 1C: PCA analysis after merging the pure lines and our genotypes
- Merge pure lines and genotypes:
  ```bash
  dir_PureLine=<Path to PureLine>
  plink --bfile <Path to genotypes> --merge-list <The name of pure line in a text file> --make-bed --out <output>
  ```

- Perform PCA on the merged data. The code for this step can be found in `eGWAS_analysis.sh`.

### Step 2: RNASeq File Preparation

#### 2A: Writing tables including gene names with mean_expression >= 1 from CPM files
- Extract and process data for each tissue. The code for this step can be found in `eGWAS_analysis.sh`.

#### 2B: Count Number of Columns and Rows
- Count rows and columns for each tissue's expression data. The code for this step can be found in `eGWAS_analysis.sh`.

### Step 3: eGWAS Analysis

#### 3A. Preparation for GCTA Analysis
- Set up directories for eGWAS outputs per tissue.

#### 3B. Preparation of Scripts to run GCTA for Different Genes
- Create 5 scripts per tissue, each including a cluster of genes:
  ```bash
  python expr_gene.py 
  ```

#### 3C: Execute GCTA Analysis
- Run GCTA analysis for each tissue using the prepared scripts. 
  ```bash
  sbatch <script_run_GCTA.sh, output of 3B>
  ```

#### 3D: Create a Summary for Genetic Parameters
- Summarize eGWAS results for each tissue. 
  ```bash
  sbatch <summary.sh for each tissue>
  ```

#### 3E: Get eGWAS Stats
- Extract significant genes based on BH adjusted p-values. 
  ```bash
  sbatch <signBH.sh for each tissue>
  ```
## Outputs of eGWAS

- **GRM Files**: Binary files containing the genetic relationship matrix (GRM) data.
- **.hsq Files**: Contains variance components and other statistics for each gene.
- **.mlma Files**: Contains association results for each gene.
- **Summary Files**: Summarized eGWAS results for each tissue.
- **signBH_genes.txt**: Summary of significant genes based on BH adjusted p-values.

---

### Step 4: Filtering and Annotating eGWAS Results

#### 4A. Convert GTF to BED format
  ```bash
  python GTF_to_bed.py <reference GTF file>
  ```

#### 4B. Filtering the SNPs in summary
- Filter the SNPs based on "N" >= 2, which represents the number of SNPs per eQTL region:
  ```bash
  python filter_summary.py <summary, output of eGWAS>
  ```

#### 4C: Creating an input including unique target genes (ensemble ID) in filtered summary file
  ```bash
  awk 'NR > 1 {print $1}' <Output of 4B> | sort | uniq > <output>
  ```

#### 4D: Annotating the gene in the filtered summary
  ```bash
  python annotate_bed.py <output of 4C>
  ```

#### 4E: Filtering sigHB_genes file resulted from eGWAS
  ```bash
  python filter_signbhN2.py <summary file from eGWAS> <signBH_genes file resulted from eGWAS>
  ```

#### 4F: Annotating the eQTLs in the file created in section 4B
  ```bash
  python .summary_annotate.py <output 4B> <output 4E>
  ```

### Step 5: eQTL and Gene Hotspots

#### 5A: Selecting the eQTLs and genes on tran-hotspot (vertical line) and horizontal line
- The codes for this step can be found in `eGWAS_analysis.sh`.

#### 5B: Counting TF and Co-F cis-genes on hotspots
  ```bash
  awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' <list of gene> Sus_scrofa_TF.txt > <output>
  awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' <list of gene> Sus_scrofa_Cof.txt > <output>
  ```

#### 5C: Selecting the 10% most significant eQTLs
  ```bash
  sort -k9,9 -g <output of 4F> | head -n <10% of each dataset> > <output>
  ```

#### 5D: eQTL-map: Plotting the SNP position vs gene position
- Create a txt file including the position of SNPs and the coordinates of associated genes:
  ```bash
  python eqtl_map_summary.py <output of 4F>
  ```

- Plot the eQTL-map. The codes for this step can be found in `eGWAS_analysis.sh`.

#### 5E: Creating a Manhattan plot for selected genes
  ```bash
  Rscript Rscript_plot.R <.mlma file resulted from eGWAS>
  ```

---

## Notes

- This pipeline utilizes tools such as `plink`, `R`, and `GCTA` for various analyses.
- Ensure that the necessary modules are loaded and paths are set correctly before executing the pipeline.
- Additional codes for extra plots used in the first paper of PPS-LIFT can be found at the end of the pipeline.

---
