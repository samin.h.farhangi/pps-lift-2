#######################################
# eGWAS analysis pipeline 
#PPS-LIFT project
#Samin HF, 27/02/2022
#######################################

#This pipeline provides a systematic procedure for eGWAS study, from genotype preparation to annotation of the results. The steps are as follows:

#1. Genotype preparation
#2. RNASeq file preparation
#3. eGWAS analysis
#4. Annotation of the generated files from eGWAS
#5. Generating plots for the first manuscript

################################ 
#Step 1: Genotype preparation
################################

#1A:Genotype QC and information update

sbatch plink.sh

DATA=/dir/6.GenotypeQC/CHR.txt #CHR.txt is including the number of chromosomes 1-18 and 23
for i in `cat $DATA`
do
 echo $i
 sbatch plink_filter.sh $i
done


#1B: PCA Analysis for Genotyping 
cd /dir/6.GenotypeQC
plink --bfile QC_100samples_filt2 --noweb --pca --out QC_100samples_filt_PCA
#We use the output named "QC_100samples_filt_PCA.eigenvec" and "QC_100samples_filt_PCA.eigenval" for the PCA_analysis


#Analyze PCA to determine the the effect of batch, alloastatic load and treatment on gene expression
#-----------------------------------------------------------------------------------------------------------------------
module load R/4.0.2
R
library("ggplot2")
library("ggrepel")
pca1 <- read.table("QC_100samples_filt_PCA.eigenvec",sep=" ",header=F)
eigenval <- read.table("QC_100samples_filt_PCA.eigenval",sep=" ",header=F)
batch <- read.table("batch_effects_info.txt",sep="\t",header=T) #This file is created based on batch, treatment and aloastatic load info
colnames(pca1)[1:2] <- c("Litter","Pig_ID")
names(pca1)[3:ncol(pca1)] <- paste0("PC", 1:(ncol(pca1)-1))
common <- merge(pca1, batch, by = c("Pig_ID"))
colnames(common)[2] <- c("Litter")
pve <- data.frame(common[3:22,], pve=eigenval/sum(eigenval)*100)
colnames(pve)[ncol(pve)] <- c("percentage")
new_col <- c("#543005","#dfc27d","#c7eae5","#35978f","#c51b7d","#f1b6da","#e6f5d0"
             ,"#4d9221","#542788","#d8daeb","#d1e5f0","#4393c3","#fdae61","#878787"
             ,"#d73027","yellow","#e7298a","#000000","#ff9999") #Define the number of colors you want
#Batch
p <- ggplot(data=common, aes(PC1,PC2, label=Pig_ID, col=as.factor(Batch))) + 
  geom_point() + 
  xlab(paste0("\nPC1: ",round(pve$percentage[1],2),"% variance")) +
  ylab(paste0("PC2: ",round(pve$percentage[2],2),"% variance\n")) + 
  scale_colour_manual(values=new_col) +
  geom_text_repel() +
  theme_classic(base_size = 18) +
  labs(color="Batch") 

png("PCA_gentoypes_batch.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

#Treatment
p<- ggplot(data=common, aes(PC1,PC2, label=Pig_ID, col=as.factor(Treatment))) + 
  geom_point() + 
  xlab(paste0("\nPC1: ",round(pve$percentage[1],2),"% variance")) +
  ylab(paste0("PC2: ",round(pve$percentage[2],2),"% variance\n")) + 
  scale_colour_manual(values=new_col[-1:-3]) +
  geom_text_repel() +
  theme_classic(base_size = 18) +
  labs(color="Treatment")
png("PCA_gentoypes_treatment.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

#Allostatic load
p<-ggplot(data=common, aes(PC1,PC2, label=Pig_ID, col=as.factor(Allostatic_load))) + 
  geom_point() + 
  xlab(paste0("\nPC1: ",round(pve$percentage[1],2),"% variance")) +
  ylab(paste0("PC2: ",round(pve$percentage[2],2),"% variance\n")) + 
  scale_colour_manual(values=new_col[-1:-7]) +
  geom_text_repel() +
  theme_classic(base_size = 18) +
  labs(color="Allostatic load")
png("PCA_gentoypes_aloastatic.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()
#---------------------------------------------------------------------------------

#1C: PCA analysis after merging the pure lines and our genotypes

#Merge pure lines and genotypes
cd /dir/6.GenotypeQC/PureLine
plink --bfile ../marta_all --merge-list all_my_files.txt --make-bed --out mymerged
#all_my_files contains: ZZZZ_LIFT, EEEE_LIFT, LLLLL_LIFT

awk 'BEGIN {OFS="\t"} {print $1, "Landrace"}' ./LLLL_LIFT.fam > L_line_ID.txt
awk 'BEGIN {OFS="\t"} {print $1, "Large_White"}' ./ZZZZ_LIFT.fam > Z_line_ID.txt
awk 'BEGIN {OFS="\t"} {print $1, "Synthetic_boar"}' ./EEEE_LIFT.fam > E_line_ID.txt
awk 'BEGIN {OFS="\t"} {print $1, "(Landrace*LargeWhite)_Synthetic_boar"}' ../marta_all/genotypes_marta_all.fam > Bred_line_ID.txt
cat L_line_ID.txt Z_line_ID.txt E_line_ID.txt Bred_line_ID.txt > mymerged_line.txt

#Perform PCA on the merged data
#-----------------------------------------------------------------------------------------------------------------------
module load R/4.0.2
R
library("ggplot2")

pca1 <- read.table("mymerged_PCA.eigenvec",sep=" ",header=F)
eigenval <- read.table("mymerged_PCA.eigenval",sep=" ",header=F)
pure_line <- read.table("mymerged_line.txt", sep="\t", header=FALSE)
colnames(pure_line) <- c("Pig_ID", "Line")
#Assign colnames
colnames(pca1)[1:2] <- c("Pig_ID", "Repeat")
names(pca1)[3:ncol(pca1)] <- paste0("PC", 1:(ncol(pca1)-1))


#Merge pca1 and pure_line based on Pig_ID
common <- merge(pca1, pure_line, by = c("Pig_ID"))


pve <- data.frame(common[3:22,], pve=eigenval/sum(eigenval)*100)
colnames(pve)[ncol(pve)] <- c("percentage")
new_col <- c("#35978f","#dfc27d","#c7eae5","#543005") #Define the number of colors you want

p <- ggplot(data=common, aes(PC1,PC2, label=Pig_ID, col=as.factor(Line))) + 
  geom_point() + 
  xlab(paste0("\nPC1: ",round(pve$percentage[1],2),"% variance")) +
  ylab(paste0("PC2: ",round(pve$percentage[2],2),"% variance\n")) + 
  scale_colour_manual(values=new_col) +
  theme_classic(base_size = 18) +
  labs(color="Line") 

jpeg("PCA_gentoypes_line.jpeg", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()
#-----------------------------------------------------------------------------------------------------------------------


################################ 
#Step 2: RNASeq file preparation
################################

#2A: Writing tables including the name of genes with  mean_expression >= 1 from CPM files 

cd /dir/6.GenotypeQC/1.eGWAS/GCTA_scripts 
cp /dir/4.Quantification/Final_counts_CPM.txt ./
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------
module load R/4.0.2
R

library(dplyr)
expr_file <- read.table("Final_counts_CPM.txt", header = TRUE, row.names = 1)

#Set tissue columns (create the list of numbers)
NCOL(expr_file)
num_liver <- seq(1, NCOL(expr_file), 4) 
num_spleen <- seq(2, NCOL(expr_file), 4)
num_lung <- seq(3, NCOL(expr_file), 4)
num_muscle <- seq(4, NCOL(expr_file), 4)

#Clean up the IDs
names(expr_file) <- gsub("WUR_", "", names(expr_file)) #remove WUR_
names(expr_file) <- gsub("_A", "", names(expr_file)) #remove _A
names(expr_file) <- gsub("_B", "", names(expr_file)) #remove _B
names(expr_file) <- gsub("_C", "", names(expr_file)) #remove _C
names(expr_file) <- gsub("_D", "", names(expr_file)) #remove _D

#Extract columns (CPM) per tissue 
expr_liver <- cbind(expr_file[,num_liver])
expr_spleen <- cbind(expr_file[,num_spleen])
expr_lung <- cbind(expr_file[,num_lung])
expr_muscle <- cbind(expr_file[,num_muscle])

#Calculating the mean of each row
expr_liver$Mean <- rowMeans(expr_liver) 
expr_spleen$Mean <- rowMeans(expr_spleen)
expr_lung$Mean <- rowMeans(expr_lung)
expr_muscle$Mean <- rowMeans(expr_muscle)

#Filter by average >=1 CPM, delete average column and transpose
expr_liver_t <- t(filter(expr_liver, Mean >= 1)[,-ncol(expr_liver)]) 
expr_spleen_t <- t(filter(expr_spleen, Mean >= 1)[,-ncol(expr_spleen)])
expr_lung_t <- t(filter(expr_lung, Mean >= 1)[,-ncol(expr_lung)])
expr_muscle_t <- t(filter(expr_muscle, Mean >= 1)[,-ncol(expr_muscle)])

#Write tables including the cpm (col) of 100 individuals (row) which their mean_expression is >= 1  
write.table(format(expr_liver_t, digits=4, scientific=F), file="Liver_expr.txt", quote = FALSE, sep="\t",col.names = FALSE)
write.table(format(expr_spleen_t, digits=4, scientific=F), file="Spleen_expr.txt", quote = FALSE, sep="\t",col.names = FALSE)
write.table(format(expr_lung_t, digits=4, scientific=F), file="Lung_expr.txt", quote = FALSE, sep="\t",col.names = FALSE)
write.table(format(expr_muscle_t, digits=4, scientific=F), file="Muscle_expr.txt", quote = FALSE, sep="\t",col.names = FALSE)

#Wite tables including the name of genes with  mean_expression >= 1  
write.table(cbind(colnames(expr_liver_t),seq.int(ncol(expr_liver_t))), file="Liver_expr_genes.txt", quote = FALSE, sep="\t",col.names = FALSE, row.names = FALSE) 
write.table(cbind(colnames(expr_spleen_t),seq.int(ncol(expr_spleen_t))), file="Spleen_expr_genes.txt", quote = FALSE, sep="\t", col.names = FALSE, row.names = FALSE) 
write.table(cbind(colnames(expr_lung_t),seq.int(ncol(expr_lung_t))), file="Lung_expr_genes.txt", quote = FALSE, sep="\t", col.names = FALSE, row.names = FALSE) 
write.table(cbind(colnames(expr_muscle_t),seq.int(ncol(expr_muscle_t))), file="Muscle_expr_genes.txt", quote = FALSE, sep="\t", col.names = FALSE, row.names = FALSE)

#Add Family ID in the first column of the gene_expression file
dir_eGWAS=/dir/6.GenotypeQC/1.eGWAS
dir_genotype=/dir/6.GenotypeQC
paste $dir_genotype/Pig_litter_info.txt $dir_eGWAS/Liver_expr.txt | awk '{$1=""; print $0}' > $dir_eGWAS/Liver_expr_final.txt
paste $dir_genotype/Pig_litter_info.txt $dir_eGWAS/Spleen_expr.txt | awk '{$1=""; print $0}' > $dir_eGWAS/Spleen_expr_final.txt
paste $dir_genotype/Pig_litter_info.txt $dir_eGWAS/Lung_expr.txt | awk '{$1=""; print $0}' > $dir_eGWAS/Lung_expr_final.txt
paste $dir_genotype/Pig_litter_info.txt $dir_eGWAS/Muscle_expr.txt | awk '{$1=""; print $0}' > $dir_eGWAS/Muscle_expr_final.txt

#2B: Count number of row and columns
# no. Rows
wc -l Liver_expr_final.txt #100
wc -l Spleen_expr_final.txt  #100
wc -l Lung_expr_final.txt #100
wc -l Muscle_expr_final.txt  #100


#no. colunms, two first col are not related to genes, they are Pig ID 
awk '{print NF}' Liver_expr_final.txt| sort -nu | tail -n 1  #12682-2= 12,680
awk '{print NF}' Spleen_expr_final.txt| sort -nu | tail -n 1 #12651-2= 12,649
awk '{print NF}' Lung_expr_final.txt| sort -nu | tail -n 1   #13312-2= 13,310 
awk '{print NF}' Muscle_expr_final.txt| sort -nu | tail -n 1 #12596-2= 12,594 


################################ 
#Step3: eGWAS analysis
################################

#3A: Preparation for GCTA Analysis
dir_genotype=/dir/6.GenotypeQC
dir_eGWAS=/dir/6.GenotypeQC/1.eGWAS

#mkdir and specifc eGWAS output scripts
mkdir /dir/6.GenotypeQC/1.eGWAS/1.Liver
mkdir /dir/6.GenotypeQC/1.eGWAS/2.Spleen
mkdir /dir/6.GenotypeQC/1.eGWAS/3.Lung
mkdir /dir/6.GenotypeQC/1.eGWAS/4.Muscle


#3B: Preparation of Scripts to run GCTA for Different Genes
cd /dir/6.GenotypeQC/1.eGWAS/GCTA_scripts

#Create 5 scripts per tissue, each including a cluster of genes
python expr_gene.py 

#Re-format the text files for unix
chmod u+x *.sh
dos2unix *.sh

#3C: Execute GCTA Analysis
#Scripts includes the command for GCTA which are set in the script_eGWAS_<tissue>_SHF.sh

sbatch 1_SHF_Liver_script_run_GCTA.sh 
sbatch 2_SHF_Liver_script_run_GCTA.sh 
sbatch 3_SHF_Liver_script_run_GCTA.sh 
sbatch 4_SHF_Liver_script_run_GCTA.sh 
sbatch 5_SHF_Liver_script_run_GCTA.sh 

sbatch 1_SHF_Lung_script_run_GCTA.sh 
sbatch 2_SHF_Lung_script_run_GCTA.sh 
sbatch 3_SHF_Lung_script_run_GCTA.sh 
sbatch 4_SHF_Lung_script_run_GCTA.sh 
sbatch 5_SHF_Lung_script_run_GCTA.sh 


sbatch 1_SHF_Spleen_script_run_GCTA.sh 
sbatch 2_SHF_Spleen_script_run_GCTA.sh 
sbatch 3_SHF_Spleen_script_run_GCTA.sh 
sbatch 4_SHF_Spleen_script_run_GCTA.sh 
sbatch 5_SHF_Spleen_script_run_GCTA.sh 


sbatch 1_SHF_Muscle_script_run_GCTA.sh 
sbatch 2_SHF_Muscle_script_run_GCTA.sh 
sbatch 3_SHF_Muscle_script_run_GCTA.sh 
sbatch 4_SHF_Muscle_script_run_GCTA.sh 
sbatch 5_SHF_Muscle_script_run_GCTA.sh 

###Outputs###
#The output are saved in /dir/6.GenotypeQC/1.eGWAS/<tissue>

###1-GRM.grm.bin###
#A binary file  contains the lower triangle elements of the GRM

###2- GRM.grm.id###
#No header line; columns are family ID and individual ID, see above

###3-GRM.grm.N.bin###
#A binary file which contains the number of SNPs used to calculate the GRM

###4-.hsq file per each gene, e.g. out_reml_<ENSSSCG00000000031>.hsq###
#Contains:
#Source  Variance        SE
#V(G)    4.792068        4.029291   #Genetic: The variation from additive genetic effects
#V(e)    7.914001        3.085100   #Residual: The variatoin from residual effects
#Vp      12.706069       2.296599   #Phenotypic: The total phenotypic variation
#V(G)/Vp 0.377148        0.277841   #Proportional: The proportion of genotypic to phenotypic variation 
#logL    -142.244        # Log Likelihood of Fitted Model
#logL0   -143.319        # Log Likelihood of Null Model
#LRT     2.150           # Likelihood Ratio Test Chi-Square 
#df      1
#Pval    0.07128         # P-value from LRT
#n       82


###5-.mlma file for each gene, e.g. out_<ENSSSCG00000000031>.mlma###
#contains:
#Chr     SNP             bp      A1      A2      Freq            b                 se             p
#1       AX-116642173    10204   A       G       0.365854        0.313124        0.678267        0.64433


#3D: Create a Summary for Genetic Parameters

#Liver
cd /dir/6.GenotypeQC/1.eGWAS/1.Liver
grep -v "inf" out_$gene_X.mlma > t_out_$gene_X.mlma #print lines that do not contain "inf"
mv t_out_$gene_X.mlma out_$gene_X.mlma
Rscript script-gcta_WG_lastP-r.r out_$gene_X.mlma sig_qval

#Spleen
cd /dir/6.GenotypeQC/1.eGWAS/2.Spleen
grep -v "inf" out_$gene_X.mlma > t_out_$gene_X.mlma #print lines that do not contain "inf"
mv t_out_$gene_X.mlma out_$gene_X.mlma
Rscript script-gcta_WG_lastP-r.r out_$gene_X.mlma sig_qval

#Lung
cd /dir/6.GenotypeQC/1.eGWAS/3.Lung
grep -v "inf" out_$gene_X.mlma > t_out_$gene_X.mlma #print lines that do not contain "inf"
mv t_out_$gene_X.mlma out_$gene_X.mlma
Rscript script-gcta_WG_lastP-r.r out_$gene_X.mlma sig_qval

#Muscle
cd /dir/6.GenotypeQC/1.eGWAS/4.Muscle
grep -v "inf" out_$gene_X.mlma > t_out_$gene_X.mlma #print lines that do not contain "inf"
mv t_out_$gene_X.mlma out_$gene_X.mlma
Rscript script-gcta_WG_lastP-r.r out_$gene_X.mlma sig_qval 

#We summarised all created *WG_for_tab_lastP per each tissue
cd /dir/6.GenotypeQC/1.eGWAS/GCTA_scripts
sbatch summary_liver.sh
sbatch summary_spleen.sh
sbatch summary_lung.sh
sbatch summary_muscle.sh

#3E: Get eGWAS Stats
#Extract significant genes based on BH adjusted p-values. 
sbatch signBH_liver.sh
sbatch signBH_spleen.sh
sbatch signBH_lung.sh
sbatch signBH_muscle.sh

####output of eGWAS####
#1) *WG_for_tab_lastP file (created by script-gcta_WG_lastP-r.r):
#In the *lastP file the snp files which are in the same win with winsize = 10,000,000 have been calculated

#2) *SNPs_Reg file (created by script-gcta_WG_lastP-r.r):
#In this file the names of all snps and their positions are saved

#3) summary (created by summary_<tissue>.sh)
# This file is summary of of all*WG_for_tab_lastP per each tissue
#1. no. Chromosome
#2. no. snps which are in the same winsize
#3. START pos of found snp in a win
#4. END pos of found snps in a win
#5. P-value
#6. lastP
#7. q-value: p.adjust(method = "BH")
#8. Bonf: p.adjust(method = "bonferroni")
#9. Beta
#10. SE
#11. Name of allel
#12. frequency of allel


#4)signBH_genes.txt (created by signBH_<tissue>.sh)
#Summary of all created *SNPs_Reg per tissue + gene name (added based onbed file of Sscrofa11.1.ens)  
#columns signBH_genes.txt are: 
#1-q-value: p.adjust(method = "BH")
#2-Bonf: p.adjust(method = "bonferroni")
#3-P-value
#4-no. Chromosome 
#5-Position of snp
#6-SNP
#7-Beta
#8-SE
#9-Name of allel
#10-frequency of allel
#11-Name of gene

#--------------------------------------------------------------------------------------------------------------------------------------------------
#Results are saved in /dir/6.GenotypeQC/1.eGWAS/signBH_counting.txt 


################################################ 
#Step 4: Filtering and Annotating eGWAS Results 
###############################################

#4A: Convert GTF to BED format

#Create a bed file for gtf file of pig
cd /dir/6.GenotypeQC/1.eGWAS/eGWAS_filter/
cp /dir/Sscrofa_genome/Sus_scrofa.Sscrofa11.1.104.gtf ./

#Output: Sus_scrofa.Sscrofa11.1.104.bed

python GTF_to_bed.py Sus_scrofa.Sscrofa11.1.104.gtf

# Count the number of protein coding and  lncRNAgenes in BED file
grep -c "protein_coding" Sus_scrofa.Sscrofa11.1.104.bed #21,280
grep -c "lncRNA" Sus_scrofa.Sscrofa11.1.104.bed #6,790


#4B: Filtering the SNPs in summary
#Filter the SNPs in summary based on "N" >= 2 which is the number of SNPs per each eQTL regions
#Output: summary_filter.txt

cp /dir/6.GenotypeQC/1.eGWAS/1.Liver/summary ./summary_Liver
cp /dir/6.GenotypeQC/1.eGWAS/2.Spleen/summary ./summary_Spleen
cp /dir/6.GenotypeQC/1.eGWAS/3.Lung/summary ./summary_Lung
cp /dir/6.GenotypeQC/1.eGWAS/4.Muscle/summary ./summary_Muscle



python filter_summary.py summary_Liver
python filter_summary.py summary_Spleen
python filter_summary.py summary_Lung
python filter_summary.py summary_Muscle


# Count associations before and after filtering
awk '{ sum += $3 } END { print sum }' summary_Liver #222858
awk '{ sum += $3 } END { print sum }' summary_Spleen #340540
awk '{ sum += $3 } END { print sum }' summary_Lung #322386
awk '{ sum += $3 } END { print sum }' summary_Muscle #200076

awk '{ sum += $3 } END { print sum }' summary_Liver_filter.txt #210466
awk '{ sum += $3 } END { print sum }' summary_Spleen_filter.txt #333652
awk '{ sum += $3 } END { print sum }' summary_Lung_filter.txt #296816
awk '{ sum += $3 } END { print sum }' summary_Muscle_filter.txt #170566


#4C: Creating an input including unique target genes (ensemble ID) in filtered summary file
# Create an input including unique target genes (ensemble ID) in summary_filter.txt
#Output: summary_filter_gene.txt
awk 'NR > 1 {print $1}' summary_Liver_filter.txt | sort | uniq > summary_Liver_filter_gene.txt
awk 'NR > 1 {print $1}' summary_Spleen_filter.txt | sort | uniq > summary_Spleen_filter_gene.txt
awk 'NR > 1 {print $1}' summary_Lung_filter.txt | sort | uniq > summary_Lung_filter_gene.txt
awk 'NR > 1 {print $1}' summary_Muscle_filter.txt | sort | uniq > summary_Muscle_filter_gene.txt

#Count the number of the genes
wc -l summary_Liver_filter_gene.txt #1003
wc -l summary_Spleen_filter_gene.txt #1258
wc -l summary_Lung_filter_gene.txt #2405
wc -l summary_Muscle_filter_gene.txt #969


#4D: Annotating the gene in the filtered summary
# Annotate summary_filter_gene.txt
#Output: summary_Liver_filter_gene_annotate.bed, summary_Spleen_filter_gene_annotate.bed, summary_Lung_filter_gene_annotate.bed, summary_Muscle_filter_gene_annotate.bed

python annotate_bed.py summary_Liver_filter_gene.txt
python annotate_bed.py summary_Spleen_filter_gene.txt
python annotate_bed.py summary_Lung_filter_gene.txt
python annotate_bed.py summary_Muscle_filter_gene.txt


# Count the number of protein coding genes in annotated BED file
grep -c "protein_coding" summary_Liver_filter_gene_annotate.bed #913
grep -c "protein_coding" summary_Spleen_filter_gene_annotate.bed #1156
grep -c "protein_coding" summary_Lung_filter_gene_annotate.bed #2284
grep -c "protein_coding" summary_Muscle_filter_gene_annotate.bed #881

#4E: Filtering sigHB_genes file resulted from eGWAS

cp /dir/6.GenotypeQC/1.eGWAS/1.Liver/signBH_genes.txt ./Liver_signBH_genes.txt
cp /dir/6.GenotypeQC/1.eGWAS/2.Spleen/signBH_genes.txt ./Spleen_signBH_genes.txt
cp /dir/6.GenotypeQC/1.eGWAS/3.Lung/signBH_genes.txt ./Lung_signBH_genes.txt
cp /dir/6.GenotypeQC/1.eGWAS/4.Muscle/signBH_genes.txt ./Muscle_signBH_genes.txt

# Filter sigHB_genes.txt including all associations based on summary files to remove association related to the regions which had 1 or 2 SNPs
#Output: filtered_Liver_signBH_genes.txt, filtered_Spleen_signBH_genes.txt, filtered_Lung_signBH_genes.txt, filtered_Muscle_signBH_genes.txt

python filter_signbhN2.py summary_Liver Liver_signBH_genes.txt
python filter_signbhN2.py summary_Spleen Spleen_signBH_genes.txt
python filter_signbhN2.py summary_Lung Lung_signBH_genes.txt
python filter_signbhN2.py summary_Muscle Muscle_signBH_genes.txt


#4F: Annotating the eQTLs in the file created in section 4B
#Annotate the eQTLs in summary_filter, clustering them to cis and trans-I and -II
#Output:  Liver_annotate_eqtl.txt, Spleen_annotate_eqtl.txt , Lung_annotate_eqtl.txt, Muscle_annotate_eqtl.txt   

python ./summary_annotate.py summary_Liver_filter.txt filtered_Liver_signBH_genes.txt
python ./summary_annotate.py summary_Spleen_filter.txt filtered_Spleen_signBH_genes.txt
python ./summary_annotate.py summary_Lung_filter.txt filtered_Lung_signBH_genes.txt
python ./summary_annotate.py summary_Muscle_filter.txt filtered_Muscle_signBH_genes.txt

#Count cis-eQTL
grep -c "cis_eQTL" ./Liver_annotate_eqtl.txt #495
grep -c "cis_eQTL" ./Spleen_annotate_eqtl.txt #800
grep -c "cis_eQTL" ./Lung_annotate_eqtl.txt #595
grep -c "cis_eQTL" ./Muscle_annotate_eqtl.txt #346


########################################## 
#Step 5. eQTL and gene Hotspots 
##########################################

#5A: Selecting the eQTLs and genes on tran-hotspot (vertical line) and horizontal line
#counting the number of cis and trans eqtls,unique genes and eQTLs on hotspots
      
#---------------------------------------------------------------
module load R/4.0.2
R

install.packages("magrittr")  
install.packages("dplyr")    
library(magrittr)  
library(dplyr) 

#dir_hotspots= /dir/6.GenotypeQC/1.eGWAS/hotspots

# Function to analyze eQTL data
analyze_eQTL_and_Gene <- function(summary_data, tissue_name) {
  
  df1 <- data.frame(summary_data)
  
  ######## Analyze eQTLs ########
  
  # Basic summary stats for eQTLs
  print(paste("Total number of eQTLs for", tissue_name, ":", length(summary_data$eqtl_id)))
  print(paste("Unique number of eQTLs for", tissue_name, ":", length(unique(summary_data$eqtl_id))))
  print(table(summary_data$cis_or_trans))
  
  # Filter eQTLs with at least 10 replicates
  df1_filter10_eqtl <- df1 %>%
    count(eqtl_id) %>%
    filter(n >= 10) %>%
    inner_join(df1, by = "eqtl_id")
  
  # Extract unique gene IDs for eQTLs
  gene_id_eqtl <- unique(df1_filter10_eqtl$gene_id)
  
  # Save eQTL data
  write.table(df1_filter10_eqtl, paste0("Merged_eQTL_", tissue_name, ".txt"), col.names = T, row.names = F, quote = F)
  write.table(gene_id_eqtl, paste0("genes_hotspots_", tissue_name, ".txt"), col.names = T, row.names = F, quote = F)
  
  ######## Analyze Genes ########
  
  # Basic summary stats for Genes
  print(paste("Total number of gene IDs for", tissue_name, ":", length(summary_data$gene_id)))
  print(paste("Unique number of gene IDs for", tissue_name, ":", length(unique(summary_data$gene_id))))
  
  # Filter genes with at least 10 replicates
  df1_filter10_gene <- df1 %>%
    count(gene_id) %>%
    filter(n >= 10) %>%
    inner_join(df1, by = "gene_id")
  
  # Extract unique gene IDs
  gene_id <- unique(df1_filter10_gene$gene_id)
  
  # Save gene data
  write.table(gene_id, paste0("genes_Horizontal_", tissue_name, ".txt"), col.names = T, row.names = F, quote = F)
  

  ######## Save cis genes in hotspots ########
  
  # Filter for cis eQTLs
  cis_id <- subset(df1_filter10_eqtl, cis_or_trans == 'cis_eQTL')
  
  # Extract unique gene IDs for cis eQTLs
  cis_unique_id <- unique(cis_id$gene_id)
  
  # Print the number of unique cis eQTLs
  print(paste("Total number of unique cis eQTLs for", tissue_name, ":", length(cis_unique_id)))
  
  # Save cis eQTL data
  write.table(cis_unique_id, paste0("cis_hotspots_", tissue_name, ".txt"), col.names = T, row.names = F, quote = F)
}

# List of tissues
tissues <- c("Liver", "Lung", "Spleen", "Muscle")

# Loop through each tissue and apply the function
for (tissue in tissues) {
  file_name <- paste0(tissue, "_annotate_eqtl.txt")
  
  # Check if the file exists
  if (file.exists(file_name)) {
    # Read the file
    summary_data <- read.table(file_name, header = TRUE, row.names = 1)
    
    # Analyze the data
    analyze_eQTL_and_Gene(summary_data, tissue)
  } else {
    # Print a message if the file doesn't exist
    cat("File", file_name, "does not exist.\n")
  }
}
#----------------------------------------------------------------

#5B: Counting TF and Co-F cis-genes on hotspots
#Counting the TFs
#Cis-genes which are TF on hotspot
awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' cis_hotspots_Liver.txt Sus_scrofa_TF.txt > cis_hotspots_TF_Liver.txt
awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' cis_hotspots_Spleen.txt Sus_scrofa_TF.txt > cis_hotspots_TF_Spleen.txt
awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' cis_hotspots_Lung.txt Sus_scrofa_TF.txt > cis_hotspots_TF_Lung.txt
awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' cis_hotspots_Muscle.txt Sus_scrofa_TF.txt > cis_hotspots_TF_Muscle.txt

#Counting the Co-Fs
#Cis-genes which are TF on hotspot
awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' cis_hotspots_Liver.txt Sus_scrofa_Cof.txt > cis_hotspots_coF_Liver.txt
awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' cis_hotspots_Spleen.txt Sus_scrofa_Cof.txt > cis_hotspots_coF_Spleen.txt
awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' cis_hotspots_Lung.txt Sus_scrofa_Cof.txt > cis_hotspots_coF_Lung.txt
awk 'NR==FNR {gene[$1]=1; next} FNR==1 {print; next} $3 in gene' cis_hotspots_Muscle.txt Sus_scrofa_Cof.txt > cis_hotspots_coF_Muscle.txt


#5C: Selecting the 10% most significant eQTLs

#Liver 10%
# Sort by the 9th column (assumed to be qvalue_most_sig) in ascending order and take the top 430 lines
sort -k9,9 -g Liver_annotate_eqtl.txt | head -n 430 > ten_percent_liver.txt
# Extract the 10rd column and get unique values
awk '{print $10}' ten_percent_liver.txt | sort -u > tenPercent_id_liver.txt
# Filter by the 7th column being 'cis_eQTL'
awk '$17=="cis_eQTL"' ten_percent_liver.txt > cis_tenpercent_liver.txt

#Spleen 10%
# Sort by the 5th column (assumed to be qvalue_most_sig) in ascending order and take the top 430 lines
sort -k9,9 -g Spleen_annotate_eqtl.txt | head -n 450 > ten_percent_spleen.txt
# Extract the 3rd column and get unique values
awk '{print $10}' ten_percent_spleen.txt | sort -u > tenPercent_id_spleen.txt
# Filter by the 7th column being 'cis_eQTL'
awk '$17=="cis_eQTL"' ten_percent_spleen.txt > cis_tenpercent_spleen.txt

#Lung 10%
# Sort by the 5th column (assumed to be qvalue_most_sig) in ascending order and take the top 430 lines
sort -k9,9 -g Lung_annotate_eqtl.txt | head -n 1060 > ten_percent_lung.txt
# Extract the 3rd column and get unique values
awk '{print $10}' ten_percent_lung.txt | sort -u > tenPercent_id_lung.txt
# Filter by the 7th column being 'cis_eQTL'
awk '$17=="cis_eQTL"' ten_percent_lung.txt > cis_tenpercent_lung.txt


#Muscle 10%
# Sort by the 5th column (assumed to be qvalue_most_sig) in ascending order and take the top 430 lines
sort -k9,9 -g Muscle_annotate_eqtl.txt | head -n 690 > ten_percent_muscle.txt
# Extract the 3rd column and get unique values
awk '{print $10}' ten_percent_muscle.txt | sort -u > tenPercent_id_muscle.txt
# Filter by the 7th column being 'cis_eQTL'
awk '$17=="cis_eQTL"' ten_percent_muscle.txt > cis_tenpercent_muscle.txt

#------------------------------------------------------------------

#5D: eQTL-map: Ploting the snp position vs gene position

#dir_eGWAS_filter= /dir/6.GenotypeQC/1.eGWAS/Hotspots
#Output:
#1-Liver_annotate_eqtl_new_pos.txt
#2-Lung_annotate_eqtl_new_pos.txt
#3- Muscle_annotate_eqtl_new_pos.txt
#4-Spleen_annotate_eqtl_new_pos.txt


#Creating a txt file icluding position of snps and coordinate of associated genes

cd /dir/6.GenotypeQC/1.eGWAS/Hotspots
python  eqtl_map_summary.py Liver_annotate_eqtl.txt
python  eqtl_map_summary.py Lung_annotate_eqtl.txt
python  eqtl_map_summary.py Spleen_annotate_eqtl.txt
python  eqtl_map_summary.py Muscle_annotate_eqtl.txt

#eQTL-map: Ploting snps pos vs associated gene start pos
#----------------------------------------------------------------------------------------
module load R/4.0.2
R
library(ggplot2)

#Liver
all_data <- read.table("Liver_annotate_eqtl_new_pos.txt", header = TRUE, row.names = 1)
plot_eqtl_liver <- ggplot(data = all_data, aes(y = gene_start , x = snp_most_sig_pos)) +
  geom_point(size = 1, shape = 20) +
  scale_y_continuous(breaks = c(0,274,426,559,690,794,965,1087,1226,1366,1435,1514,1576,1784,1926,2066,2146,2210,2266,2392),
                     labels = c("" ,1,"",3,"",5,"",7,"",9,"",11,"",13,"",15,"",17,"","X")) +
  scale_x_continuous(breaks = c(0,274,426,559,690,794,965,1087,1226,1366,1435,1514,1576,1784,1926,2066,2146,2210,2266,2392),
                     labels = c("" ,1,"",3,"",5,"",7,"",9,"",11,"",13,"",15,"",17,"","X")) +
  scale_alpha_manual(values=c(0.5))+
  theme_classic() +
  theme(axis.text = element_text(size=14),axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),panel.background = element_rect(fill = scales::alpha("#d7191c", 0.5))) +  
  labs(x=" ", y=" ")
ggsave("Liver_eQTL_map.png", plot = plot_eqtl_liver, width = 15, height = 15, units = "cm")

#Lung
all_data <- read.table("Lung_annotate_eqtl_new_pos.txt", header = TRUE, row.names = 1)
plot_eqtl_lung <- ggplot(data = all_data, aes(y = gene_start , x = snp_most_sig_pos)) +
  geom_point(size = 1, shape = 20) +
  scale_y_continuous(breaks = c(0,274,426,559,690,794,965,1087,1226,1366,1435,1514,1576,1784,1926,2066,2146,2210,2266,2392),
                     labels = c("" ,1,"",3,"",5,"",7,"",9,"",11,"",13,"",15,"",17,"","X")) +
  scale_x_continuous(breaks = c(0,274,426,559,690,794,965,1087,1226,1366,1435,1514,1576,1784,1926,2066,2146,2210,2266,2392),
                     labels = c("",1,"",3,"",5,"",7,"",9,"",11,"",13,"",15,"",17,"","X")) +
  scale_alpha_manual(values=c(0.5))+
  theme_classic() +
  theme(axis.text = element_text(size=14),axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),panel.background = element_rect(fill = scales::alpha("#abdda4", 0.5))) +  
  labs(x=" ", y=" ")
ggsave("Lung_eQTL_map.png", plot = plot_eqtl_lung, width = 15, height = 15, units = "cm")

#Spleen
all_data <- read.table("Spleen_annotate_eqtl_new_pos.txt", header = TRUE, row.names = 1)
plot_eqtl_spleen <- ggplot(data = all_data, aes(y = gene_start , x = snp_most_sig_pos)) +
  geom_point(size = 1, shape = 20) +
  scale_y_continuous(breaks = c(0,274,426,559,690,794,965,1087,1226,1366,1435,1514,1576,1784,1926,2066,2146,2210,2266,2392),
                     labels = c("" ,1,"",3,"",5,"",7,"",9,"",11,"",13,"",15,"",17,"","X")) +
  scale_x_continuous(breaks = c(0,274,426,559,690,794,965,1087,1226,1366,1435,1514,1576,1784,1926,2066,2146,2210,2266,2392),
                     labels = c("" ,1,"",3,"",5,"",7,"",9,"",11,"",13,"",15,"",17,"","X")) +
  scale_alpha_manual(values=c(0.5))+
  theme_classic() +
  theme(axis.text = element_text(size=14),axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),panel.background = element_rect(fill = scales::alpha("#fdae61", 0.5))) +  
  labs(x=" ", y=" ")
ggsave("Spleen_eQTL_map.png", plot = plot_eqtl_spleen, width = 15, height = 15, units = "cm")

#Muscle
all_data <- read.table("Muscle_annotate_eqtl_new_pos.txt", header = TRUE, row.names = 1)
plot_eqtl_muscle <- ggplot(data = all_data, aes(y = gene_start , x = snp_most_sig_pos)) +
  geom_point(size = 1, shape = 20) +
  scale_y_continuous(breaks = c(0,274,426,559,690,794,965,1087,1226,1366,1435,1514,1576,1784,1926,2066,2146,2210,2266,2392),
                     labels = c("" ,1,"",3,"",5,"",7,"",9,"",11,"",13,"",15,"",17,"","X")) +
  scale_x_continuous(breaks = c(0,274,426,559,690,794,965,1087,1226,1366,1435,1514,1576,1784,1926,2066,2146,2210,2266,2392),
                     labels = c("" ,1,"",3,"",5,"",7,"",9,"",11,"",13,"",15,"",17,"","X")) +
  scale_alpha_manual(values=c(0.5))+
  theme_classic() +
  theme(axis.text = element_text(size=14),axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),panel.background = element_rect(fill = scales::alpha("#2b83ba", 0.5))) +  
  labs(x=" ", y=" ")
ggsave("Muscle_eQTL_map.png", plot = plot_eqtl_muscle, width = 15, height = 15, units = "cm")
#----------------------------------------------------------------------------------------  
  
#5E: Creating a Manhattan plot for selected genes

cp /dir/6.GenotypeQC/1.eGWAS/2.Spleen/out_ENSSSCG00000003070.mlma
mv out_ENSSSCG00000003070.mlma out_ENSSSCG00000003070_znf45.mlma
cp /dir/6.GenotypeQC/1.eGWAS/2.Spleen/out_ENSSSCG00000032154.mlma 
mv out_ENSSSCG00000032154.mlma out_ENSSSCG00000032154_erf.mlma

Rscript Rscript_plot.R  out_ENSSSCG00000003070_znf45.mlma
Rscript Rscript_plot.R  out_ENSSSCG00000032154_erf.mlma


################################################
#Extra plots for the paper
################################################

#dir = /dir/6.GenotypeQC/1.eGWAS/extra_plots
cd /dir/6.GenotypeQC/1.eGWAS/extra_plots


#A. Sup Figure 5
#Distribution of eQTL intervals resulting from eGWAS analysis by length

#First we created a excel file including the length of eQTLs for four tissues
#------------------------------------------------------------------------------
module load R/4.0.2
R

library(ggplot2)
library(gridExtra)

df <- read_excel("eQTL_length.xlsx")

library(readxl)
library(ggplot2)


plot1 <- ggplot(df, aes(x = eqtl_length_Liver)) +
  geom_density(fill = "#d7191c", alpha = 0.5) +
  theme_classic() +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank()) +
  labs(x = "\neQTL length", y = "Density\n", title = "") +
  scale_x_continuous(breaks = c(0.0e+00, 2.5e+06, 5e+06, 7.5e+06, 1.0e+07),
                     labels = c("0", "2.5Mb", "5Mb", "7.5Mb", "10Mb"))

plot2<-ggplot(df, aes(x = eqtl_length_lung)) +
  geom_density(fill = "#abdda4", alpha = 0.5) +
  theme_classic() +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank()) +
  labs(x = "\neQTL length", y = "Density\n", title = "") +
  scale_x_continuous(breaks = c(0.0e+00, 2.5e+06, 5e+06, 7.5e+06, 1.0e+07),
                     labels = c("0", "2.5Mb", "5Mb", "7.5Mb", "10Mb"))

plot3<-ggplot(df, aes(x = eqtl_length_spleen)) +
  geom_density(fill = "#fdae61", alpha = 0.5) +
  theme_classic() +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank()) +
  labs(x = "\neQTL length", y = "Density\n", title = "") +
  scale_x_continuous(breaks = c(0.0e+00, 2.5e+06, 5e+06, 7.5e+06, 1.0e+07),
                     labels = c("0", "2.5Mb", "5Mb", "7.5Mb", "10Mb"))
plot4<-ggplot(df, aes(x = eqtl_length_muscle)) +
  geom_density(fill = "#2b83ba", alpha = 0.5) +
  theme_classic() +
  theme(axis.text.y = element_blank(), axis.ticks.y = element_blank()) +
  labs(x = "\neQTL length", y = "Density\n", title = "") +
  scale_x_continuous(breaks = c(0.0e+00, 2.5e+06, 5e+06, 7.5e+06, 1.0e+07),
                     labels = c("0", "2.5Mb", "5Mb", "7.5Mb", "10Mb"))

# Now you can use this with ggsave
ggsave("eQTL_intervals_Liver.jpeg", plot = plot1, width = 6, height = 4, dpi = 300)
ggsave("eQTL_intervals_Lung.jpeg", plot = plot2, width = 6, height = 4, dpi = 300)
ggsave("eQTL_intervals_Spleen.jpeg", plot = plot3, width = 6, height = 4, dpi = 300)
ggsave("eQTL_intervals_Muscle.jpeg", plot = plot4, width = 6, height = 4, dpi = 300)

#----------------------------------------------------------------------------------

#B. 
#Distribution of distance between the position of most significant SNP and its associated gene in cis-eQTL and trans-eQTL-I intervals 
# We first convert the eQTL_annotate files to excel file
#--------------------------------------------------------
module load R/4.0.2
R
library(ggplot2)
library(readxl)
library(cowplot)


# Define custom theme for plots
custom_theme <- theme_classic() +
  theme(axis.text.y = element_blank(),
        axis.ticks.y = element_blank(),
        axis.text.x = element_text(size = 18))

# Function to generate plots
generate_plot <- function(data, fill_color) {
  ggplot(data, aes(x = distance_SNP_start)) +
    geom_density(fill = fill_color, alpha = 0.5) +
    custom_theme +
    labs(x = "", y = "", title = "")
}

# Read and process data for each tissue
tissues <- c("Liver", "Lung", "Spleen", "Muscle")
colors  <- c("#d7191c", "#abdda4", "#fdae61", "#2b83ba")

for (i in 1:length(tissues)) {
  tissue_data <- data.frame(read_excel(paste0(tissues[i], "_annotate_eqtl.xlsx")))
  tissue_data$distance_SNP_start <- as.numeric(tissue_data$distance_SNP_start)
  
  # cis plot
  sub_data_cis <- subset(tissue_data, cis_or_trans == "cis_eQTL")
  plot_cis <- generate_plot(sub_data_cis, colors[i]) +
    scale_x_continuous(breaks = c(-1e+06,0,1e+06), labels = c("-1Mb", "0Mb", "1Mb"))
  ggsave(paste0("DisPlot_", tissues[i], "_cis.jpeg"), plot = plot_cis, width = 8, height = 6, dpi = 300)
  
  # trans plot
  
  sub_data_trans_neg <- subset(tissue_data, cis_or_trans == "trans_eQTLI" & distance_SNP_start <= -1e6)
  sub_data_trans_pos <- subset(tissue_data, cis_or_trans == "trans_eQTLI" & distance_SNP_start >= 1e6)
  
  plot_trans_neg <- generate_plot(sub_data_trans_neg, colors_trans[i]) +
    scale_x_continuous(limits = c(min(sub_data_trans_neg$distance_SNP_start), -1e6), 
                       breaks = c(-1e+08,-1e+06), labels = c("-100Mb", "-1Mb")) +
    theme(axis.text.x = element_text(size = 18), 
          axis.text.y = element_blank(), axis.ticks.y = element_blank(),
          axis.line.y = element_blank())
  
  plot_trans_pos <- generate_plot(sub_data_trans_pos, colors_trans[i]) +
    scale_x_continuous(limits = c(1e6, max(sub_data_trans_pos$distance_SNP_start)),
                       breaks = c(1e+06, 1e+08), labels = c("1Mb", "100Mb"))+
    theme(axis.text.x = element_text(size = 18), 
          axis.text.y = element_blank(), axis.ticks.y = element_blank(),
          axis.line.y = element_blank())
  plot_trans_combined <- plot_grid(plot_trans_neg, plot_trans_pos, ncol = 2, align = "h", rel_widths = c(1, 1))
  
  ggsave(paste0("DisPlot_", tissues[i], "_trans.jpeg"), plot = plot_trans_combined, width = 8, height = 6, dpi = 300)
}
  
#--------------------------------------------------------------------------------------------


#5I.
#ANOVA test to check q-value and cis- and trans-eqtls

#ANOVA test to effect of cis/trans on eQTL value
#Input: three coloumns, 1)number 2)qvalue 3)trans/cis (extracted from annotated summary)
#---------------------------------------------------------------------
module load R/4.0.2
R
library("readxl")
library(ggplot2)
install.packages("agricolae")
library(agricolae)

#Liver:
my_data <- read_excel("ANOVA.xlsx", sheet = "ANOVA_Liver")
#Anova and LSD test
model<-aov(qvalue~group, data=my_data)
summary(model)
mean(my_data$qvalue)
df<-df.residual(model)
#LSD test
MSerror<-deviance(model)/df
out<-LSD.test(model, "group", console = TRUE)
plot(out)
summary(out)
#convert the file to a matrix
my_data$group <-as.factor(my_data$group)
liver_plot <- ggplot(my_data, aes(x=group, y=qvalue)) +
  geom_boxplot(fill="#d7191c", color="black") +
  theme_classic() +
  scale_x_discrete(limits=c("cis_eQTL", "trans_eQTL-I", "trans_eQTL-II"), 
                   labels = c("cis eQTL", "trans eQTL-I", "trans eQTL-II"))+
  theme(axis.text.x = element_text(color = "black", size = 14),
        axis.text.y = element_text(color = "black", size = 12),
        axis.title.y = element_text(color = "black", size = 16)) +
  labs(title = "", x = "", y = "Significant association (q-value)\n")
ggsave("ANOVA_liver.png", plot = liver_plot, width = 19.4, height = 10, units = "cm")


#Lung:
my_data <- read_excel("ANOVA.xlsx", sheet = "ANOVA_Lung")
#Anova and LSD test
model<-aov(qvalue~group, data=my_data)
summary(model)
mean(my_data$qvalue)
df<-df.residual(model)
#LSD test
MSerror<-deviance(model)/df
out<-LSD.test(model, "group", console = TRUE)
plot(out)
summary(out)
#convert the file to a matrix
my_data$group <-as.factor(my_data$group)
lung_plot <- ggplot(my_data, aes(x=group, y=qvalue)) +
  geom_boxplot(fill="#abdda4", color="black") +
  theme_classic() +
  scale_x_discrete(limits=c("cis_eQTL", "trans_eQTL-I", "trans_eQTL-II"), 
                   labels = c("cis eQTL", "trans eQTL-I", "trans eQTL-II"))+
  theme(axis.text.x = element_text(color = "black", size = 14),
        axis.text.y = element_text(color = "black", size = 12),
        axis.title.y = element_text(color = "black", size = 16)) +
  labs(title = "", x = "", y = "Significant association (q-value)\n")


ggsave("ANOVA_lung.png", plot = lung_plot, width = 19.4, height = 10, units = "cm")

#Spleen:
my_data <- read_excel("ANOVA.xlsx", sheet = "ANOVA_Spleen")
#Anova and LSD test
model<-aov(qvalue~group, data=my_data)
summary(model)
mean(my_data$qvalue)
df<-df.residual(model)
#LSD test
MSerror<-deviance(model)/df
out<-LSD.test(model, "group", console = TRUE)
plot(out)
summary(out)
#convert the file to a matrix
my_data$group <-as.factor(my_data$group)
spleen_plot <- ggplot(my_data, aes(x=group, y=qvalue)) +
  geom_boxplot(fill="#fdae61", color="black") +
  theme_classic() +
  scale_x_discrete(limits=c("cis_eQTL", "trans_eQTL-I", "trans_eQTL-II"), 
                   labels = c("cis eQTL", "trans eQTL-I", "trans eQTL-II"))+
  theme(axis.text.x = element_text(color = "black", size = 14),
        axis.text.y = element_text(color = "black", size = 12),
        axis.title.y = element_text(color = "black", size = 16)) +
  labs(title = "", x = "", y = "Significant association (q-value)\n")

ggsave("ANOVA_spleen.png", plot = spleen_plot, width = 19.4, height = 10, units = "cm")

#Muscle:
my_data <- read_excel("eGWAS_extra_files.xlsx", sheet = "ANOVA_Muscle")
#Anova and LSD test
model<-aov(qvalue~group, data=my_data)
summary(model)
mean(my_data$qvalue)
df<-df.residual(model)
#LSD test
MSerror<-deviance(model)/df
out<-LSD.test(model, "group", console = TRUE)
plot(out)
summary(out)
#convert the file to a matrix
my_data$group <-as.factor(my_data$group)
muscle_plot <- ggplot(my_data, aes(x=group, y=qvalue)) +
  geom_boxplot(fill="#2b83ba", color="black") +
  theme_classic() +
  scale_x_discrete(limits=c("cis_eQTL", "trans_eQTL-I", "trans_eQTL-II"), 
                   labels = c("cis eQTL", "trans eQTL-I", "trans eQTL-II"))+
  theme(axis.text.x = element_text(color = "black", size = 14),
        axis.text.y = element_text(color = "black", size = 12),
        axis.title.y = element_text(color = "black", size = 16)) +
  labs(title = "", x = "", y = "Significant association (q-value)\n")

ggsave("ANOVA_muscle.png", plot = muscle_plot, width = 19.4, height = 10, units = "cm")

#-----------------------------------------------------------------
