#!/usr/bin/env python
from sys import argv

"""
@author: hosse004; 07-04-2022

This program parses eQTL annotated files and generates a file to plot
the absolute SNP position (in Mb) against the absolute gene position (in Mb).
"""

def parse_eqtl_file(eqtl_file):
    """Parses the eQTL annotated file to extract relevant information.
    
    Parameters:
    eqtl_file (file): A file handle to the eQTL annotated file.
    
    Returns:
    list: A list of lists, where each inner list contains the chromosome and position for both SNP and gene.
    """
    
    eqtl_list = []
    
    # Define the offsets for each chromosome to convert to absolute positions
    chr_dict = {'1': 0, '2': 274.33, '3': 426.27, '4': 559.12, '5': 690.03, 
                '6': 794.56, '7': 965.4, '8': 1087.24, '9': 1226.21, '10': 1365.72,
                '11': 1435.08, '12': 1514.25, '13': 1575.85, '14': 1784.19, 
                '15': 1925.95, '16': 2066.36, '17': 2146.3, '18': 2209.79, 
                'X': 2265.77}
    
    # Parse the eQTL file line by line
    for line in eqtl_file:
        if not line.startswith("eqtl"):
            eqtl = line.strip().split()
            
            chr_snp = eqtl[2]
            pos_sig_snp = int(eqtl[6])
            snp_mb_pos = float(pos_sig_snp / 1e6) + float(chr_dict[chr_snp])
            
            chr_gene = eqtl[10]
            gene_start = int(eqtl[11])
            gene_mb = float(gene_start / 1e6)
            
            if chr_gene in chr_dict.keys():
                gene_mb_pos = gene_mb + float(chr_dict[chr_gene])
                eqtl_list.append([chr_snp, snp_mb_pos, chr_gene, gene_mb_pos])
                
    return eqtl_list


def write_output(eqtl_list, outname):
    """Writes the parsed eQTL information into an output file.
    
    Parameters:
    eqtl_list (list): A list of lists containing parsed eQTL data.
    outname (str): The name of the output file.
    """
    
    with open(outname, 'w') as out:
        out.write('\tchr_snp\tsnp_most_sig_pos\tgene_chr\tgene_start\n')
        n = 0
        for rec in eqtl_list:
            n += 1
            out.write(f"{n}\t{rec[0]}\t{rec[1]}\t{rec[2]}\t{rec[3]}\n")


if __name__ == "__main__":
    # Input file name from command line argument
    infile_name = argv[1]
    
    # Open and parse the input file
    with open(infile_name, 'r') as infile:
        eqtl_list = parse_eqtl_file(infile)
    
    # Generate the output file name and write the output
    out_name = infile_name[:-4] + "_new_pos.txt"
    write_output(eqtl_list, out_name)
