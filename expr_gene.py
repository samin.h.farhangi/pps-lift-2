#!/usr/bin/env python

"""
@author: hosse004; 2-27-2022

This program takes an expression genes file for a specified tissue, which contains gene names in the first column and 
gene numbers in the second. It prepares bash files to run eGWAS in 5 separate files for each tissue.

Input example for Liver: 'Liver_expr_genes.txt'
Output example for Liver: 1_Liver_expr_genes.sh; 2_Liver_expr_genes.sh; 3_Liver_expr_genes.sh; 4_Liver_expr_genes.sh; 5_Liver_expr_genes.sh
"""

def write_headers_to_file(file_obj):
    """
    Writes the common header information to the provided file object.
    """
    file_obj.write("#!/bin/sh\n#SBATCH --time=10-25:00:00\n#SBATCH -N 1\n#SBATCH -c 1\n#SBATCH --mem=15000\n")
    file_obj.write("#SBATCH --qos=Std\n#SBATCH --output=slurm.output_%j.txt\n#SBATCH --error=slurm.error_%j.txt\n")
    file_obj.write("dir_eGWAS_liver=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS/1.Liver\n")


def write_output(expr_file, tissue_type):
    """
    Create and write to output bash files based on the input data for a specific tissue.
    """
    # Create the output files
    output_files = [open(f"{i}_{tissue_type}_expr_genes.sh", 'w') for i in range(1, 6)]
    
    # Write headers to each file
    for file_obj in output_files:
        write_headers_to_file(file_obj)

    # Process each line of the input file and write to the appropriate output file
    for line in expr_file:
        gene_name, no_gene = line.strip().split()
        no_gene = int(no_gene)

        # Determine which file to write to based on the value of no_gene
        if no_gene < 2600:
            target_file = output_files[0]
        elif no_gene < 5200:
            target_file = output_files[1]
        elif no_gene < 7800:
            target_file = output_files[2]
        elif no_gene < 10400:
            target_file = output_files[3]
        else:
            target_file = output_files[4]

        target_file.write(f'echo {gene_name} {no_gene} cov_X.txt {tissue_type}_expr_final.txt | srun script_eGWAS_{tissue_type}_SHF.sh >> $dir_eGWAS_liver/out_eGWAS.txt\n')

    # Close all output files
    for file_obj in output_files:
        file_obj.write("\n")
        file_obj.close()


if __name__ == "__main__":
    tissues = ["Liver", "Lung", "Spleen", "Muscle"]

    for tissue_type in tissues:
        infile_name = f'{tissue_type}_expr_genes.txt'
        
        with open(infile_name, 'r') as infile:
            write_output(infile, tissue_type)
