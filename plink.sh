#!/bin/bash
#SBATCH --time=25:00:00
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=4000
#SBATCH --qos=Std
#SBATCH --output=slurm.output_%j.txt

dir_rawgenotype=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/marta_all
dir_genotype=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC


module load plink/1.9-180913


#Update Individual IDs and Family IDs - 100 samples
plink --bfile $dir_rawgenotype/genotypes_marta_all --recode --update-ids $dir_genotype/file_update_ids_100.txt --make-bed --out $dir_genotype/QC_100samples_upd_ids

#Update sex information
plink --bfile $dir_genotype/QC_100samples_upd_ids --recode --update-sex $dir_genotype/file_update_sex_100.txt --make-bed --out $dir_genotype/QC_100samples_upd_ids_sex

#Extract only samples from our study (proper individual ID)
plink --bfile $dir_genotype/QC_100samples_upd_ids_sex --recode --make-bed --keep $dir_genotype/file_keep_samples.txt --out $dir_genotype/QC_100samples_upd
