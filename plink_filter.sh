#!/bin/bash
#SBATCH --time=25:00:
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=4000
#SBATCH --qos=Std
#SBATCH --output=slurm.output_%j.txt

dir_rawgenotype=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/marta_all
dir_genotype=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC


module load plink/1.9-180913

#Filter snps
#exclude SNPs missing genotype for 95% of the samples 
#exclude individuals missing 10% of their genotypes
#For each chromosome separately 
sname=$1
plink --bfile $dir_genotype/QC_100samples_upd \
--maf 0.01 \   #Allele frequency	
--geno 0.1 \   #Missingness per marker	
--mind 0.1 \   #Missingness per individual	
--hwe 1e-12 \  #Hardy-Weinberg equilibrium	
--chr ${sname} \
--keep-allele-order \
--make-grm-gz \
--make-bed --out $dir_genotype/QC_100samples_filt_CHR_${sname}

#For all chromosomes
plink --bfile $dir_genotype/QC_100samples_upd \
--maf 0.01 \   #Allele frequency	
--geno 0.1 \   #Missingness per marker	
--mind 0.1 \   #Missingness per individual	
--hwe 1e-12 \  #Hardy-Weinberg equilibrium	
--chr 1-18, 23 \
--keep-allele-order \
--make-grm-gz \
--make-bed --out $dir_genotype/QC_100samples_filt2
