module load R/4.0.2

args<-commandArgs(TRUE)

library(data.table)

a<-fread(args[1],header=TRUE,data.table=FALSE)

##Chr   SNP	bp	A1	A2	Freq	b	se	p
g<-data.frame(a$Chr,a$SNP,a$bp,a$p,a$b,a$se,a$A1,a$Freq)

colnames(g)<-c("CHR","SNP","BP","P","Beta","es","A1","freq")

c2q<-p.adjust(g$P,method = "BH")

Bc1q<-p.adjust(g$P,method = "bonferroni")

e<-data.frame(c2q,Bc1q,g["P"],g["CHR"],g["BP"],g["SNP"],g["Beta"],g["es"],g["A1"],g["freq"])

colnames(e)<-c("q","BON","P","CHR","BP","SNP","Beta","es","A1","freq") # p-bonferroni = BON and p-BH = q

data<-e[e$q<=0.05,] #subset the data with p-BH > 0.05

colnames(data)<-c("q","Bonf","P","Chr","Pos","SNP","Beta","es","A1","freq") #column names!! it depends on which is the database

snps_reg<-matrix(nrow=0,ncol=13) #create a matrix in which you save the data after processing               
Fullsnps<-matrix(nrow=0,ncol=10)
winsize=10000000     #windowsize 10 Milion

#circle to analyze all the chromosomes
for (chr in 1:19)
{
pos1=0   #initial bp position of analisys
#Matrix with only data for a specified chromosome
    c <- data[(data[,"Chr"]==chr),]
    c <- c[order(c[,"Chr"]),]
    if (dim(c)[1]>0){
    m=max(c[,"Pos"])
#cwin=1

#Calculate windows of size winsize, mean homozygosity, number of SNP per window
    while ( pos1<=m)
    {
        pos2 = pos1+winsize #last position of the window
        w <-c[(c[,"Pos"]>=pos1)&(c[,"Pos"]<pos2),] #matrix with only those snp that are in the window you want
        if (dim(w)[1]>0){
			print(w)
        w<-w[order(w[,"Pos"]),]
        qval_ord<-w[order(w[,"q"]),] ## order per q-value
        qval_most<-qval_ord[1,] #significat
        first<-head(w,n=1) # start positions
        last<-tail(w,n=1)  # end positions
        alltab<-c(qval_most[,4],dim(w)[1],as.character(qval_most[,6]),first[,5],last[,5],qval_most[,3],last[,3],round(qval_most[,1],8),round(qval_most[,2],8),qval_most[,7],qval_most[,8],as.character(qval_most[,9]),qval_most[,10])
     
        
        #allSNPs_reg<-as.data.frame(as.character(w[,6]))
        allSNPs_reg<-w

        
        
        snps_reg<-rbind(snps_reg,alltab)
        
        ### extract all name of snps in each region Fullsnps<-matrix(nrow=0,ncol=1)
       
        print(Fullsnps)
        
        print(allSNPs_reg)
        Fullsnps<-rbind(Fullsnps,allSNPs_reg)
        
        
    }
        
        pos1=pos2        #set the new initial position quita el 1

    }
    }

}

colnames(Fullsnps)<-c("q","Bonf","P","Chr","pos","SNP","Beta","es","A1","freq")

colnames(snps_reg)<- c("CHR","N","SNP","START","STOP","P","lastP","q","Bonf","Beta","es","A1","freq")
if (dim(snps_reg)[1]>0){
write.table(snps_reg,paste(args[2],args[1],"WG_for_tab_lastP",sep="_"),col.names=T,row.names=F,quote=F)
write.table(Fullsnps,paste(args[2],args[1],"SNPs_Reg",sep="_"),col.names=T,row.names=F,quote=F)

print("Finsih")
}

echo "End FDR correction"
