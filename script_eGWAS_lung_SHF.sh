#!/bin/sh

#We do not include quanitative covariables
read -p "Please enter 4 variable: 
1- Name of gene
2- Number of column where gene is in the expression file
3- cov_x
4- Name of the expression file: " gene_X gene_n cov_X genefile_X

#Calculate relationship matrix from SNPs
module load gcta/1.25.3
dir_genotype=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC
dir_eGWAS=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS
dir_eGWAS_out=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS/2.Spleen

gcta64 --bfile $dir_genotype/QC_100samples_filt2 \
--autosome-num 23 \
--make-grm \
--out $dir_eGWAS_out/GRM \
--thread-num 5



#reml calculation: To generate an estimate of variance explained by the GRM adjusted for all fixed effects
gcta64 --reml --grm $dir_eGWAS_out/GRM \
--pheno $genefile_X \
--covar $cov_X \
--mpheno $gene_n \
--autosome-num 23 \
--out $dir_eGWAS_out/out_reml_$gene_X \
--thread-num 5


#Mixed model association GWAS 
gcta64 --mlma \
--bfile $dir_genotype/QC_100samples_filt2 \
--autosome-num 23 \
--grm $dir_eGWAS_out/GRM \
--pheno $genefile_X \
--covar $cov_X \
--mpheno $gene_n \
--thread-num 5 \
--out $dir_eGWAS_out/out_$gene_X