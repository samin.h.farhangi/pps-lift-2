#!/bin/sh
#SBATCH --time=10-25:00:00
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=5000
#SBATCH --qos=Std
#SBATCH --output=slurm.output_%j.txt

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS/1.Liver 
#Summary of significant SNPs
for i in `ls *_lastP` ; do awk -v i=$i '{print i,$0}' $i > borrar_$i; done
tail -n +1 borrar_* | grep -v "CHR" | grep -v "==>" | grep . | sed 's/sig_qval_out_//g' | sed 's/.mlma_WG_for_tab_lastP//g' > summary ; rm borrar_*
#Summary for genetic paramters
for i in `ls *.hsq`; do 
sed -n 1,5p $i | \
awk -v i=$i '{print i,$0}' | \
sed 's/out_reml_//g' | sed 's/.hsq//g' >> summary_VarG ; done


for i in `ls *_lastP` ; do awk -v i=$i '{print i,$0}' $i > borrar_$i; done
tail -n +1 borrar_* | grep -v "CHR" | grep -v "==>" | grep . | sed 's/sig_qval_out_//g' | sed 's/.mlma_WG_for_tab_lastP//g' > summary rm borrar_*
